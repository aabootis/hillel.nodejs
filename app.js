'use strict';

require('dotenv').config();
const http = require('http');
const fs = require('fs');
const path = require('path');
const util = require('util');
const EventEmitter = require('events');
const readChunk = require('read-chunk');
const fileType = require('file-type');
const moment = require('moment');

const normalizePort = require('./modules/normalizePort');
const ClassLogger = require('./modules/logger');
const Logger = new ClassLogger();
const ClassAnalytics = require('./modules/analytics');
const analytics = new ClassAnalytics();

const bigFile = path.join(__dirname, process.env.FILE || 'LICENSE');
const PORT = normalizePort(process.env.PORT || 3000);


setInterval(() => {
    Logger.log = analytics.log;
}, 60000);

const server = new http.Server();

server.on('request', (req, res) => {
    setTimeout(() => {
        if(req.method === 'GET') {
            if (req.url === '/') {
                fs.open(bigFile, 'r', async (error, fd) => {
                    if (error) {
                        if (error.code === 'ENOENT') {
                            res.writeHead(404);
                            res.end('File not found');
                            Logger.log = 'Запрошенный файл не найден\n';
                        } else {
                            res.writeHead(500);
                            res.end(error);
                            Logger.log = 'Ошибка получения доступа к файлу\n';
                        }
                        return;
                    } else {
                        const buffer = await readChunk.sync(bigFile, 0, 4100);
                        const currentFileType = fileType(buffer);

                        if (currentFileType) {
                            res.writeHead(200, {
                                "Content-type": fileType(buffer).mime
                            });
                        } else {
                            res.writeHead(200, {
                                "Content-type": "text/plain"
                            });
                        }

                        const fileStream = fs.createReadStream(bigFile);
                        fileStream.on('open', () => {
                            Logger.emit('starttimer');
                            Logger.log = 'Начинаем отдавать файл\n';
                            fileStream.pipe(res);
                        });

                        /*fileStream.on('end', () => {});*/


                        fileStream.on('close', () => {
                            if (typeof(fileStream.destroy) === 'function') {
                                fileStream.destroy();
                            }
                        });

                        res.on('error', (e) => {
                            console.log(e);
                        });

                        res.on('finish', () => {
                            Logger.log = 'Закончили отдавать файл (успешно). Потрачено ' + (moment().valueOf() - Logger.timer)/1000 + 'сек.\n';
                            analytics.emit('request', 'Успешно');
                        });

                        res.on('close', () => {
                            Logger.log = 'Закончили отдавать файл (прервано клиентом). Потрачено ' + (moment().valueOf() - Logger.timer)/1000 + 'сек.\n';
                            analytics.emit('request', 'Прервано клиентом');
                            if (typeof(fileStream.destroy) === 'function') {
                                fileStream.destroy();
                            }
                        });
                    }
                });


            } else {
                res.writeHead(404);
                res.end('Request URL not found');
                Logger.log = 'Запрошенный файл не найден\n';
            }
        } else {
            res.writeHead(405);
            res.end('Request method not allowed');
            Logger.log = 'Метод запроса не поддерживается\n';
        }
    }, 0);
});

server.on('error', (error) => {
    if (error.syscall !== "listen") {
        throw error;
    }
    let bind = typeof PORT === "string" ? "Pipe " + PORT : "Port " + PORT;
    // handle specific listen errors with friendly messages
    switch (error.code) {
    case "EACCES":
        console.error(bind + " requires elevated privileges");
        Logger.log = 'Недостаточно прав для запуска сервера\n';
        process.exit(1);
        break;
    case "EADDRINUSE":
        console.error(bind + " is already in use");
        Logger.log = 'Необходимый для запуска сервера порт занят\n';
        process.exit(1);
        break;
    default:
        throw error;
    }
});

server.on('listening', () => {
    let addr = server.address();
    let bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
    console.log("Listening on " + bind);
    Logger.log = 'Сервер запущен\n';
});

server.listen(PORT);
