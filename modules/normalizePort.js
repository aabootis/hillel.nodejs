/**
 * Normalize a port into a number, string, or false.
 * @param {string} val - Port value
 * @return {number} Normalized port value
 */

module.exports = (val) => {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}
