'use strict';

const fs = require('fs');
const path = require('path');
const moment = require('moment');
const { EventEmitter } = require('events');

const logFile = path.join(__dirname, '..', 'logs', 'access.log');
class ClassLogger extends EventEmitter {
    constructor() {
        super();

        this.loggerStream = fs.createWriteStream(logFile, {
            flags: 'w',
            encoding: 'utf8',
            autoClose: true
        });

        this.on('starttimer', () => {
            this.timer = moment().valueOf();
        });
    }

    set log(message) {
        this.loggerStream.write('[' + moment().format('Do MMMM YYYY, hh:mm:ss') + '] ' + message + '\r\n');
    }
}

module.exports = ClassLogger;
