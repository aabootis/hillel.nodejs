'use strict';

const EventEmitter = require('events');

class ClassAnalytics extends EventEmitter {
    constructor () {
        super();

        this.count = 0;
        this.statuses = [];

        this.on('request', (status) => {
            this.count++;
            this.statuses.push(status);
        });
    }

    get log() {
        let count = this.count;
        this.count = 0;
        let statuses = this.statuses.toString()
        this.statuses = [];
        return `Количество запросов: ${count}, статусы: ${statuses}`;
    }
}

module.exports = ClassAnalytics;
